// pages/sign/sign.js
import withBehaviors from '../../utils/withBehaviors'
import loginBehavior from '../../behaviors/loginBehavior'

const chooseLocation = requirePlugin('chooseLocation')

const key = 'O62BZ-VK6KF-TVPJ7-NTL3G-QW44S-OFF3T'

const referer = "A股IPO排队列表"

const category = '生活服务,娱乐休闲';


const date = new Date()
const years = []
const months = []
const days = []
const hours = []
const endHours =[]

for (let i = 1990; i <= date.getFullYear(); i++) {
  years.push(i)
}

for (let i = 1; i <= 12; i++) {
  months.push(i)
}

for (let i = 1; i <= 31; i++) {
  days.push(i)
}

for (let i = 0; i<=23; i++) {
  hours.push(i)
}

for (let i = 1; i<=24; i++) {
  endHours.push(i)
}

const app = getApp()

Page(withBehaviors([loginBehavior])(
  {

    /**
     * 页面的初始数据
     */
    data: {
      years,
      year: date.getFullYear(),
      months,
      month: date.getMonth() + 1,
      days,
      day: date.getDate(),
      hours,
      hour:date.getHours(),
      endHours,
      endHour: date.getHours() + 1,
    },
  
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
  
    },
  
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
  
    },
  
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
      const signSuccess = app.globalData.signSuccess
      if(signSuccess){
        app.globalData.signSuccess = false
        this.setData({
          location: false
        })
      }else{
        const location = chooseLocation.getLocation();
        console.log(location)
        if(location !== null){
          const month = date.getMonth()
          const day = date.getDate()-1
          const hour = date.getHours()
          this.setData({
            address: location.name,
            location: location,
            value: [].concat(999,month,day,hour, hour)
          })
        }
      }
    },
  
    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {
  
    },
  
    handleSignIn(){
      wx.navigateTo({
        url: 'plugin://chooseLocation/index?key=' + key + '&referer=' + referer  + '&category=' + category
      });
    },
  
    handleChangeLocation(){
      const location = JSON.stringify({
        latitude: this.data.location.latitude,
        longitude: this.data.location.longitude
      })
      wx.navigateTo({
        url: 'plugin://chooseLocation/index?key=' + key + '&referer=' + referer + '&location=' + location + '&category=' + category
      });
    },
  
    handlePickerChange(e) {
      const val = e.detail.value
      this.setData({
        year: this.data.years[val[0]],
        month: this.data.months[val[1]],
        day: this.data.days[val[2]],
        hour: this.data.hours[val[3]],
        endHour: this.data.endHours[val[4]]
      })
    },
  
    handleSignConfirm() {
      const {address, year, month, day, hour, endHour} = this.data
      const wxml = `
        <view class="container" >
          <view class="item-box title">
            <text class="text">防范疫情，人人有责</text>
          </view>
          <view class="item-box">
            <text class="text">我在${year}年${month}月${day}日${hour}时至${endHour}时到过公共场所</text>
          </view>
          <view class="item-box location">
            <text class="text">${address}</text>
          </view>
          <view class="item-box number">
            <text class="text">共15人到过此地</text>
          </view>
          <image class="image" src="/assets/QRcode.jpg" /> 
          <view class="item-box">
            <text class="text footer">注：签到有助于后续提供给医疗机构做流行病学史分析，帮助更好的控制疫情</text>
          </view>
        </view>
        `
      app.globalData.wxml = wxml
      wx.navigateTo({
        url: '/pages/sign_success/sign_success',
      })

    },
  }
)

)