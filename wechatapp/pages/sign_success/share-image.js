
const style = {
  container: {
    width: 414,
    height: 400,
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: "#fff"
  },
  itemBox: {
    width: 330,
    height: 50,
    textAlign: 'center'
  },
  title: {
    fontSize: 20,
    color: '#c04851'
  },
  location: {
    fontSize: 18,
  },
  test: {
    backgroundColor: "#ccc",
    height: 150,
    textAlign: "center"
  },
  image: {
    width: 120,
    height: 120,
  },
  footer: {
    color: "#666"
  },
  text: {
    width: 330,
    height: 60,
    textAlign: 'center',
    verticalAlign: 'middle',
  },
  number: {
    color: '#c04851',
    fontSize: 16,
  }

}

  module.exports = { style }
