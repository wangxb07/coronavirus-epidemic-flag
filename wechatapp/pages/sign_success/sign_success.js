// pages/sign_success/sign_success.js
import {style} from './share-image'

const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    isCanvasCreate: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
   this.widget = this.selectComponent('.widget')
   app.globalData.signSuccess = true
   this.setData({
     wxml: app.globalData.wxml
   })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    setTimeout(()=>{
    const wxml = this.data.wxml
    const p1 = this.widget.renderToCanvas({ wxml, style })
    p1.then((res) => {
      console.log('container', res.layoutBox)
      this.container = res
    })
    }, 100)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  handleShareImage() {
    const p2 = this.widget.canvasToTempFilePath()
    p2.then(res => {
      wx.previewImage({
        urls: [res.tempFilePath],
        current: res.tempFilePath
      })
      this.setData({
        src: res.tempFilePath,
        width: this.container.layoutBox.width,
        height: this.container.layoutBox.height
      })
    })

  }
})