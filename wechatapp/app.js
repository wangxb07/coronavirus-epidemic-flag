//app.js
import { get as locationFlagGet, create as locationFlagCreate } from './resources/openapi/location_flag'

App({
  onLaunch: function () {
    // todo remove 示例
    locationFlagGet(2).then((res) => {
      console.log(res)
    })
  },
    /**
   * App全局事件topcis
   */
  eventTopics: {},

  /**
   * App全局事件订阅
   */
  eventSub(topic, cb) {
    if (typeof this.eventTopics[topic] === 'undefined') {
      this.eventTopics[topic] = []
    }
    this.eventTopics[topic].push(cb)
  },

  /**
   * App全局事件发布
   */
  eventPub(topic, payload = {}) {
    if (typeof this.eventTopics[topic] !== 'undefined') {
      this.eventTopics[topic].forEach((cb) => {
        cb(payload)
      })
    }
  },

  /**
   * App全局事件移除订阅
   */
  eventRm(topic, cb) {
    if (typeof this.eventTopics[topic] === 'undefined') {
      return
    }

    let cbIndex = -1
    this.eventTopics[topic].forEach((element, index) => {
      if (element === cb) {
        cbIndex = index
      }
    })

    if (cbIndex >= 0) {
      this.eventTopics[topic].splice(cbIndex, 1)
    }
  },

  /**
   * App全局事件移除所有订阅
   */
  eventRmAll(topic) {
    if (typeof this.eventTopics[topic] !== 'undefined') {
      this.eventTopics[topic].length = 0
    }
  },
  globalData: {
    userInfo: null,
    signSuccess: false,
  }
})
