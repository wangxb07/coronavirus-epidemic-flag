import jwtDecode from '../miniprogram_npm/jwt-decode/index'

import {
  userLoginByCode, setUserInfo, getUserInfo, setCookie,
} from '../resources/auth'

const app = getApp()


const loginBehavior = {
  name: 'loginBehavior',

  data: {

  },

  behaviorData: {

  },

  onLoad(options) {
    this.validateLogin().then((res) => {
      if (typeof this.afterLogin === 'function') {
        this.afterLogin(options, res)
      }
    })
  },

  methods: {
    validateLogin() {
      return new Promise((resolve) => {
        const userInfo = getUserInfo()
        // 判断用户信息是否存在
        if (userInfo && userInfo.user) {
          const isExp = this.checkEpr()
          // 判断用户信息是否过期
          if (isExp) {
            // const { refreshToken } = userInfo
            // refreshLoginInfo(refreshToken)
            this.loginByCode()
          }
          else {
            app.eventPub('authCheckSuccess')
          }
        }
        else {
          this.loginByCode()
        }
        resolve()
      })
    },

    // 通过code码向后端拿 sessname & sessid
    loginByCode() {
      const p = new Promise((resolve, reject) => {
        wx.login({
          success: (res) => {
            console.log(res)
            userLoginByCode(res.code).then((r) => {
              const id = r.user.id
              if (id == 0) {
                const cookie = `${r.sess_name}=${r.sess_id}`
                setCookie(cookie)
                app.eventPub('authPhoneValidate')
              }
              else {
                setUserInfo(r)
                resolve(r)
              }
            })
          },
        })
      })

      return p.then(() => {
        app.eventPub('authCheckSuccess')
      })
    },
    // 判断是否过期
    checkEpr() {
      const identifying = getUserInfo()
      if (identifying) {
        const exp = jwtDecode(identifying.jwt).exp
        const currentTime = Math.floor(Date.now() / 1000)
        if (exp > currentTime) {
          return false
        }
        return true
      }
    },

  },
}

module.exports = loginBehavior
