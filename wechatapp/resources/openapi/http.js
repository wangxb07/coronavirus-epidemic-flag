import { HttpClient } from '../../utils/fetcher'
import { URLBuilder } from '../../utils/urlBuilder'

import { apiHost } from '../../config/global'

export const builder = new URLBuilder(apiHost)

builder.addGlobalQueryParam("_format", "schema_json:json")

export const http = new HttpClient((builder))
export default http
