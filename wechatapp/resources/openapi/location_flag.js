import http from './http'

/**
 * openapi definition
 *
 * 接口详细文档查阅此链接。
 *
 * https://app.swaggerhub.com/apis-docs/Beehplus/drupal-nCov-flag/1.0.0
 */

/**
 * 读取单条数据接口
 *
 * @param id
 * @returns {*}
 */
export function get(id) {
  return http.anonymousRequest(`/rest/location-flag/${id}`)
}

/**
 * 创建接口
 *
 * @param body
 *  {
 *    "name": "xxxxx",
 *    "lat": 0,
 *    "lon": 0
 *  }
 * @returns {*}
 */
export function create(body) {
  return http.authedRequest('/rest/location-flag', {
    method: 'POST',
    data: body,
  }, true)
}
