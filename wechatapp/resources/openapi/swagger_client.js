// const Swagger = require('../libs/swagger-client.js')
// const spec = {
//   "swagger": "2.0",
//   "schemes": [
//     "https"
//   ],
//   "info": {
//     "description": "The REST API provide by the core REST module.",
//     "title": "Coronavirus Epidemic Flag - REST API",
//     "version": "1.0.0"
//   },
//   "host": "master-7rqtwti-4n6h27wejj3by.au.platformsh.site",
//   "basePath": "/",
//   "securityDefinitions": {
//     "oauth2": {
//       "type": "oauth2",
//       "description": "For more information see https://developers.getbase.com/docs/rest/articles/oauth2/requests",
//       "flow": "password",
//       "tokenUrl": "https://master-7rqtwti-4n6h27wejj3by.au.platformsh.site/oauth/token",
//       "scopes": {
//         "node": "node content access"
//       }
//     },
//     "csrf_token": {
//       "type": "apiKey",
//       "name": "X-CSRF-Token",
//       "in": "header",
//       "x-tokenUrl": "https://master-7rqtwti-4n6h27wejj3by.au.platformsh.site/user/token"
//     }
//   },
//   "security": [{
//     "oauth2": []
//   },
//     {
//       "csrf_token": []
//     }
//   ],
//   "tags": [{
//     "name": "locationFlag",
//     "description": "Entity type: Content as location flag"
//   }],
//   "definitions": {},
//   "consumes": [
//     "application/json",
//     "application/schema+json:json"
//   ],
//   "produces": [
//     "application/json",
//     "application/schema+json:json"
//   ],
//   "paths": {
//     "/rest/location-flag/{id}": {
//       "get": {
//         "parameters": [{
//           "name": "_format",
//           "in": "query",
//           "type": "string",
//           "enum": [
//             "schema_json:json",
//             "json"
//           ],
//           "required": true,
//           "description": "Request format"
//         },
//           {
//             "name": "id",
//             "type": "string",
//             "in": "path",
//             "required": true
//           }
//         ],
//         "responses": {
//           "200": {
//             "description": "successful operation"
//           },
//           "400": {
//             "description": "Bad request",
//             "schema": {
//               "type": "object",
//               "properties": {
//                 "error": {
//                   "type": "string",
//                   "example": "Bad data"
//                 }
//               }
//             }
//           },
//           "500": {
//             "description": "Internal server error.",
//             "schema": {
//               "type": "object",
//               "properties": {
//                 "message": {
//                   "type": "string",
//                   "example": "Internal server error."
//                 }
//               }
//             }
//           }
//         },
//         "tags": [
//           "locationFlag"
//         ],
//         "summary": "Location flag rest resource",
//         "operationId": "getLocationFlag",
//         "schemes": [
//           "http"
//         ],
//         "security": [{
//           "oauth2": []
//         }]
//       }
//     },
//     "/rest/location-flag": {
//       "post": {
//         "parameters": [{
//           "name": "_format",
//           "in": "query",
//           "type": "string",
//           "enum": [
//             "schema_json:json",
//             "json"
//           ],
//           "required": true,
//           "description": "Request format"
//         }, {
//           "name": "body",
//           "in": "body",
//           "description": "The Content object",
//           "required": true,
//           "schema": {
//             "type": "object",
//             "properties": {
//               "name": {
//                 "type": "string",
//                 "example": "xxxxx"
//               },
//               "lat": {
//                 "type": "number"
//               },
//               "lon": {
//                 "type": "number"
//               }
//             }
//           }
//         }],
//         "responses": {
//           "200": {
//             "description": "successful operation"
//           },
//           "400": {
//             "description": "Bad request",
//             "schema": {
//               "type": "object",
//               "properties": {
//                 "error": {
//                   "type": "string",
//                   "example": "Bad data"
//                 }
//               }
//             }
//           },
//           "500": {
//             "description": "Internal server error.",
//             "schema": {
//               "type": "object",
//               "properties": {
//                 "message": {
//                   "type": "string",
//                   "example": "Internal server error."
//                 }
//               }
//             }
//           }
//         },
//         "tags": [
//           "locationFlag"
//         ],
//         "summary": "Location flag rest resource",
//         "operationId": "createLocationFlag",
//         "schemes": [
//           "http"
//         ],
//         "security": [{
//           "oauth2": []
//         }]
//       }
//     }
//   }
// }
//
// export const createSwaggerClient = async (accessToken) => {
//   const client = await Swagger({
//     spec,
//     authorizations: {
//       oauth2: {
//         token: {
//           access_token: accessToken
//         }
//       }
//     },
//   })
//
//   return client
// }
