// import {createSwaggerClient} from './swagger_client'
//
// describe('swagger-client', () => {
//   let client;
//
//   beforeAll(async (done)  => {
//     client = await createSwaggerClient("eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjUyZTJhMGFkOGU1ODMwNjAxMjQ3OTQ5NThjMTFlZjEwZDUxMDkyOWQ5YzVmMTYxOTU1OTA1Y2ZhZjE4OGI1ZTU5Yzc2Yjc1OTNjNjM3NDUxIn0.eyJhdWQiOiJjZGU3YzEzNy02N2Y2LTQwYzUtYTc1Ny05ZDE3OWFmMTQwN2EiLCJqdGkiOiI1MmUyYTBhZDhlNTgzMDYwMTI0Nzk0OTU4YzExZWYxMGQ1MTA5MjlkOWM1ZjE2MTk1NTkwNWNmYWYxODhiNWU1OWM3NmI3NTkzYzYzNzQ1MSIsImlhdCI6MTU4MDgwMTA2MSwibmJmIjoxNTgwODAxMDYxLCJleHAiOjE1ODA4MDEzNjEsInN1YiI6IjEiLCJzY29wZXMiOlsiYXV0aGVudGljYXRlZCJdfQ.mZKDtjMjjKyfTk0LcZG3h7FUejasB0icZAUsUD3lCc2AzdSZDKE2S6H4jKZQUbOHwwFoYWLn7NQVj2EyKGeyou4QeU_KT54lLzMS9Po6dATEzh_oWtylTWV9Lgo5O27fUFiEZvtHgftP05XNVuRd5Mb89cmnGJCBVDrgCa-Bkk7wqgm5Tj0CPvYLBSRc1-sS3FZqMiAmWg1d0EmFcRCk2wt0GgVAW6ArcZalZ0apdQm_KP8zyWzBnKivgwRoDTiwfhumhLzJCcFcjc3uQFSo78rXqi5VE7lMNHLPnbe1_1F0G1EJaq0RA1Ufd8TgAka0Oh1mkg2PVn9kHJTzDBH4vkDGRugZTOIIEbzXI3OOJAn6hX4Co_CfiMBIPe83XXwiFc0BdQzPobFzidGdrj2noTg2mbYJhiwaSfSJkJw255fjfmhX8IRjvojwZywG8bB-BHOoW4CBDmQp-8p5y7QEDUct7_E_tMvsUNeHwjoYr-Q2jGiW2YvWsC_xpehCLu0lisx4nK-Pmb8s2QZIpc5Of_sB_H5d6KNA6C6CllU3Pg6Re6EP9pwgqlv8mZlEcBTn1aK1GMI5wVi0buK0qHA09fsW5tS4rEB7RI_TEHspL3OhG-Rrz_U4kwu-yETEhWLdS0f95hrdQ-yCksdrznxyCOBlu5fgU68fhDLlMVzjgmw")
//     done()
//   })
//
//   test('rest node user trace get', async () => {
//     const res = await client.apis.locationFlag.getLocationFlag({id: 2, _format: 'schema_json:json'})
//     console.log(JSON.parse(res.data.toString('utf8')))
//   }, 10000)
//
//   test('rest node user trace create', async () => {
//     const node = await client.apis.locationFlag.createLocationFlag({
//       body: {
//         "name": "新的内容",
//         "lat": "45.909621",
//         "lon": "6.127147",
//       },
//       _format: 'schema_json:json'
//     })
//
//     console.log(node)
//   }, 10000)
// })
