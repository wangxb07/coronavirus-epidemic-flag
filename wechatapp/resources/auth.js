import http from '../utils/http'
import { appId,  apiHost} from '../config/global'

export const userLoginByCode = (code, authenticate = true) => {
  const url = '/auth/wechat_login_by_code'

  let data = { code }

  if (authenticate !== true) {
    data = { code, authenticate: 0 }
  }
  return http.anonymousRequest(url, {
    method: 'POST',
    header: {
      'content-type': 'application/x-www-form-urlencoded', // 默认值
      'X-BEEHPLUS-STORE-ID': appId
    },
    data,
  }).then((response) => {
    return response.data
  })
}

export const userLoginStatus = () => {
  const url = '/user/login_status?_format=json'
  return http.authedRequest(url, {
    header: {
      Accept: 'application/json',
      'X-WECHAT-APPID': appId
    },
  }).then((response) => response.data)
}

export const userPhoneNumber = (iv, encryptedData, cookie) => {
  const url = '/auth/wechat_phone_quick_login'
  return http.anonymousRequest(url, {
    method: 'post',
    header: {
      'content-type': 'application/x-www-form-urlencoded', // 默认值
      'X-BEEHPLUS-STORE-ID': appId,
      Cookie: cookie,
      'X-WECHAT-APPID': appId
    },
    data: {
      iv: encodeURIComponent(iv),
      encrypted_data: encodeURIComponent(encryptedData),
    },
  }).then((response) => response.data)
}

/*
 *将用户相关数据写入缓存
 * TODO 处理参数结构，优化调用方法
 */
export const setUserInfo = (res) => {
  const user = res.user || ''
  const sessid = res.sess_id || ''
  const session_name = res.sess_name || ''
  const jwt = res.jwt || ''
  const refreshToken = res.refresh_token || ''
  return wx.setStorageSync('identify', {
    sessid, session_name, user, jwt, refreshToken,
  })
}

/*
*将用户手机授权所需的cookie存入缓存
*/
export const setCookie = (cookie) => wx.setStorageSync('cookie', cookie)


/*
*读取用户相关数据
*/
export const getUserInfo = () => {
  const value = wx.getStorageSync('identify')
  if (value) {
    return value
  }
  return false
}