// components/layouts/layout_coupon.js
import { userPhoneNumber, setUserInfo } from '../../resources/auth'

const app = getApp()

Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    displayAuthorizeButton: false,
    loading: true,
    loadingContentList: ['auth'],
  },

  /**
   * 生命周期函数
   */
  lifetimes: {
    // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
    attached() {
      app.eventSub('authCheckSuccess', this.updateLoading.bind(this, 'auth'))
      app.eventSub('authPhoneValidate', this.updateAuthButton.bind(this))
    },
  },

  /**
   * 组件的方法列表
   */
  methods: {

    updateAuthButton() {
      this.setData({
        loading: false,
        displayAuthorizeButton: true,
      })
    },

    updateLoading(name) {
      this.data.loadingContentList = this.data.loadingContentList.filter((item) => item != name)
      if (this.data.loadingContentList.length === 0) {
        this.setData({
          loading: false,
        })
      }
    },

    handleGetPhoneNumber(e) {
      if (typeof e.detail.iv !== 'undefined' && typeof e.detail.encryptedData !== 'undefined') {
        wx.showLoading({
          title: '加载中',
          mask: true,
        })
        wx.login({
          success: () => {
            const cookie = wx.getStorageSync('cookie')
            if (cookie) {
              userPhoneNumber(e.detail.iv, e.detail.encryptedData, cookie).then((r) => {
                this.setData({
                  displayAuthorizeButton: false,
                })
                setUserInfo(r)
                wx.hideLoading()
              })
            }
            else {
              wx.showToast({
                title: '系统错误，请重试',
                icon: 'none',
                duration: 1500,
              })
              wx.hideLoading()
            }
          },
        })
      }
    },
  },
})

