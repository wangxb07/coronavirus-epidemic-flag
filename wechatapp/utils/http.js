import { apiHost } from "../config/global"

class httpClient {
  constructor(host) {
    this.host = host
    this.identityKey = 'identity'
  }

  setHost(host) {
    this.host = host
  }

  setIdentityKey(key) {
    this.identityKey = key
  }

  fetch(path, options, withToken = false) {
    const host = this.host
    if (!withToken) {
      return new Promise((resolve, reject) => {
        return wx.request({
          url: host + path,
          ...options,
          success: function (response) {
            if (response.statusCode === 200) {
              resolve(response)
            }
            else {
              reject(response)
            }
          },
          fail: function (response) {
            reject(response)
          }
        })
      })
    }
    else {
      return new Promise((resolve, reject) => {
        wx.request({
          url: host + 'api/user/token',
          method: 'POST',
          header: {
            ...options.header
          },
          success: function (response) {
            if (response.statusCode === 200) {
              options.header = Object.assign({}, {
                'X-CSRF-Token': response.data.token
              }, options.header)

              wx.request({
                url: host + path,
                ...options,
                success: function (response) {
                  if (response.statusCode === 200) {
                    resolve(response)
                  }
                  else {
                    reject(response)
                  }
                },
                fail: function (response) {
                  reject(response)
                }
              })
            }
          }
        })
      })
    }
  }

  anonymousRequest(path, options = {}) {
    options.header = Object.assign({}, {
      'Accept': 'application/json'
    }, options.header)

    return this.fetch(path, options)
  }

  authedRequest(path, options = {}, withToken = false) {
    const identity = wx.getStorageSync(this.identityKey) || {}
    if (typeof identity.sessid === 'undefined') {
      // TODO 当用户未登录情况下，调用authed request 将被触发一个权限异常
      console.error('授权请求' + path + '时，身份信息丢失')
      throw 'Identity miss'
    }

    options.header = Object.assign({}, {
      'Accept': 'application/json',
      'Cookie': identity.session_name + '=' + identity.sessid,
    }, options.header)

    return this.fetch(path, options, withToken)
  }
}

const http = new httpClient(apiHost)

module.exports = http
