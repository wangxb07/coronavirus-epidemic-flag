const _ = require('../libs/lodash.js')

const withBehaviors = (behaviors, options = {}) => (pageConfig) => {
  const mergeData = (a, behavior) => _.merge(a, behavior.data)
  let methods = {}


  let data = _.merge({}, behaviors.reduce(mergeData, {}), pageConfig.data)

  behaviors.forEach((behavior) => {
    data = _.merge({}, data, {
      [behavior.name]: behavior.behaviorData,
    })

    methods = _.merge({}, methods, behavior.methods)
  })

  const lifeCyclies = [
    'onLoad',
    'onReady',
    'onShow',
    'onHide',
    'onUnload',
    'onPullDownRefresh',
    'onReachBottom',
    'onshareAppMessage',
  ]

  lifeCyclies.forEach((lifeCycleName) => {
    if (typeof pageConfig[lifeCycleName] === 'undefined') {
      // TODO 冗余代码1
      pageConfig[lifeCycleName] = function (pageOptions) {
        behaviors.forEach((behavior) => {
          if (typeof behavior[lifeCycleName] === 'function') {
            behavior[lifeCycleName].call(this, pageOptions)
          }
        })
      }
    }
    else {
      const origin = pageConfig[lifeCycleName]

      // TODO 冗余代码1
      pageConfig[lifeCycleName] = function (pageOptions) {
        behaviors.forEach((behavior) => {
          if (typeof behavior[lifeCycleName] === 'function') {
            behavior[lifeCycleName].call(this, pageOptions)
          }
        })
        origin.call(this, pageOptions)
      }
    }
  })
  return _.assign({}, {
    ...methods,
  },
  pageConfig, {
    data,
  })
}

module.exports = withBehaviors
